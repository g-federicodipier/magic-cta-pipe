#!/usr/bin/env python
# coding: utf-8

import argparse
import yaml

import os

__description__ = 'Create a yaml configuration file for the analysis with magic_cta_pipe.'
formatter = argparse.ArgumentDefaultsHelpFormatter
PARSER = argparse.ArgumentParser(description=__description__, formatter_class=formatter)
PARSER.add_argument('-r', '--root_dir', type=str, required=False, default='/storage/gpfs_data/ctalocal/dipierr/data/MAGIC_LST/Crab_campaign', help='directory where you want to create the source directory')
PARSER.add_argument('-s', '--source_name', type=str, required=False, default='CrabNebula', help='name of the source')
PARSER.add_argument('-d', '--date', type=str, required=False, default='2020-01-19', help='date in format YYYY-MM-DD')
PARSER.add_argument('-t', '--tag', type=str, required=False, default='v1', help='Tag for the analysis of defined Source, Observation date')
PARSER.add_argument('--create_dir', default=False, dest='create_dir',action='store_true',help='The program also can create the directories needed for the analysis, if the option is set (with no argument) the program will attempt')

def main(**kwargs):
	root_dir =  kwargs['root_dir']
	source_name =  kwargs['source_name']
	date = kwargs['date']
	tag = kwargs['tag']

	source_dir = (f"{root_dir}/{source_name}/{date}/{tag}")
	
	create_output_dir = kwargs['create_dir']

	data = dict(
	    data_files = dict(
	    	mc = dict(
	    		train_sample = dict(
	    			magic1 = dict(
	    				input_mask = "/storage/gpfs_data/ctalocal/aberti/MAGIC_LST/Analysis/Crab_campaign/CrabNebula/2019-11-27/Calibrated_MC/Diffuse_ST0311/Train/M1/GA_M1*root",
	    				hillas_output = (f"{source_dir}/Hillas/MC/MC_train_hillas_M1.h5")
	    			),
					magic2 = dict(
	    				input_mask = "/storage/gpfs_data/ctalocal/aberti/MAGIC_LST/Analysis/Crab_campaign/CrabNebula/2019-11-27/Calibrated_MC/Diffuse_ST0311/Train/M2/GA_M2*root",
	    				hillas_output = (f"{source_dir}/Hillas/MC/MC_train_hillas_M2.h5")
	    			)
	    		),
				test_sample = dict(
					magic1 = dict(
	    				input_mask = "/storage/gpfs_data/ctalocal/aberti/MAGIC_LST/Analysis/Crab_campaign/CrabNebula/2019-11-27/Calibrated_MC/Diffuse_ST0311/Test/M1/GA_M1*root",
	    				hillas_output = (f"{source_dir}/Hillas/MC/MC_test_hillas_M1.h5"),
	    				reco_output = (f"{source_dir}/Reco/MC/MC_test_reco_M1.h5")
	    			),
					magic2 = dict(
	    				input_mask = "/storage/gpfs_data/ctalocal/aberti/MAGIC_LST/Analysis/Crab_campaign/CrabNebula/2019-11-27/Calibrated_MC/Diffuse_ST0311/Test/M2/GA_M2*root",
	    				hillas_output = (f"{source_dir}/Hillas/MC/MC_test_hillas_M2.h5"),
	    				reco_output = (f"{source_dir}/Reco/MC/MC_test_reco_M2.h5")
	    			)
	    		),
	    	),
	    	data = dict(
	    		train_sample = dict(
	    			magic1 = dict(
	    				input_mask = "/storage/gpfs_data/ctalocal/aberti/MAGIC_LST/Analysis/Crab_campaign/CrabNebula/2019-11-27/Calibrated_OFF/M1/20*_M1_*.root",
	    				hillas_output = (f"{source_dir}/Hillas/OFF/OFF_train_hillas_M1.h5")
	    			),
					magic2 = dict(
	    				input_mask = "/storage/gpfs_data/ctalocal/aberti/MAGIC_LST/Analysis/Crab_campaign/CrabNebula/2019-11-27/Calibrated_OFF/M2/20*_M1_*.root",
	    				hillas_output = (f"{source_dir}/Hillas/OFF/OFF_train_hillas_M2.h5")
	    			)
	    		),
				test_sample = dict(
					magic1 = dict(
	    				input_mask = "/storage/gpfs_data/ctalocal/aberti/MAGIC_LST/Analysis/Crab_campaign/CrabNebula/2020-01-19/Calibrated_ON/M1/20*_M1_*root",
	    				hillas_output = (f"{source_dir}/Hillas/ON/ON_test_hillas_M1.h5"),
	    				reco_output = (f"{source_dir}/Reco/ON/ON_test_reco_M1.h5")
	    			),
					magic2 = dict(
	    				input_mask = "/storage/gpfs_data/ctalocal/aberti/MAGIC_LST/Analysis/Crab_campaign/CrabNebula/2020-01-19/Calibrated_ON/M2/20*_M2_*root",
	    				hillas_output = (f"{source_dir}/Hillas/ON/ON_test_hillas_M2.h5"),
	    				reco_output = (f"{source_dir}/Reco/ON/ON_test_reco_M2.h5")
	    			)
	    		),

	    	),	
	    ),

		image_cleaning = dict(
			magic = dict(
				charge_thresholds = dict(
					picture_thresh=6,
					boundary_thresh=3.5,
					min_number_picture_neighbors=1
				),
				time_thresholds = dict(
					# 1.5 slices x 1.64 GHz
		            max_time_diff=2.46,
		            # 4.5 slices x 1.64 GHz
		            max_time_off=7.38,
		            min_number_neighbors=1	
				)
			)
		), 

		energy_rf = dict(
			save_name=source_dir+"/RFs/energy_rf.joblib",
    		cuts="(multiplicity > 1) & (intensity > 30) & (length > 0.0) & (intensity_width_1 < 0.15)",
    		settings = dict(
				n_estimators=100,
		        min_samples_leaf=10,
		        n_jobs=3
		    ),
		    features=['slope', 'length', 'width', 'intensity', 'skewness', 'kurtosis', 'x', 'y', 'psi', 'n_islands']
		    
    	),

		direction_rf = dict(
			save_name=source_dir+"/RFs/direction_rf.joblib",
		    cuts="(multiplicity > 1) & (intensity > 30) & (length > 0.0) & (intensity_width_1 < 0.15)",
		    settings=dict(
		        n_estimators=100,
		        min_samples_leaf=10,
		        n_jobs=3
		    ),
		    features=dict(
		        disp=['slope', 'length', 'width', 'intensity', 'skewness', 'kurtosis', 'x', 'y', 'psi'],
		        pos_angle_shift=['slope', 'length', 'width', 'intensity', 'skewness', 'kurtosis', 'x', 'y', 'psi']
		    )
		),

		classifier_rf = dict(
			save_name=source_dir+"/RFs/classifier_rf.joblib",
		    cuts='(multiplicity > 1) & (intensity > 30) & (length > 0.0) & (intensity_width_1 < 0.15)',
		    settings=dict(
		        n_estimators=100,
		        min_samples_leaf=10,
		        n_jobs=3
		    ),
		    features=['slope', 'length', 'width', 'intensity', 'skewness', 'kurtosis', 'x', 'y', 'psi', 'n_islands']
		),

		event_list = dict(
			max_time_diff=6.9e-4,
		    cuts=dict(
		        quality=dict(
		            l3rate="80 < value < 1000",
		            dc="0 < value < 7e3"
		        ),
		        selection="(multiplicity > 1) & (abs(pos_angle_shift_reco - 0.5) > 0.4) & (event_class_0 > 0.8)"
		    )
		)
	)
	output_filename=("%s_%s.yaml"%(source_name,date))
	with open(output_filename, 'w') as outfile:
	    yaml.dump(data, outfile, sort_keys=False, default_flow_style=False, indent=4, width=100)  

	if (create_output_dir):
		hillas_output_dir_mc=(f"{source_dir}/Hillas/MC")
		reco_output_dir_mc=(f"{source_dir}/Reco/MC")
		hillas_output_dir_off=(f"{source_dir}/Hillas/OFF")
		hillas_output_dir_on=(f"{source_dir}/Hillas/ON")
		reco_output_dir_on=(f"{source_dir}/Reco/ON")
		rfs_dir=(f"{source_dir}/RFs")
		mynewdirs = [hillas_output_dir_mc,reco_output_dir_mc,hillas_output_dir_off, hillas_output_dir_on,reco_output_dir_on,rfs_dir]
		for dire in mynewdirs:
			MYDIR=dire
			CHECK_FOLDER = os.path.isdir(MYDIR)
			if not CHECK_FOLDER:
			    os.makedirs(MYDIR)
			    print(f"created folder : {MYDIR}")
			else:
   				print(f"{MYDIR} folder already exists.")

if __name__ == '__main__':
    args = PARSER.parse_args()
    main(**args.__dict__)