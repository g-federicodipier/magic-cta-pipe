#!/usr/bin/env python
# coding: utf-8

import argparse
import yaml

__description__ = 'Read a yaml configuration file for the analysis with magic_cta_pipe.'
formatter = argparse.ArgumentDefaultsHelpFormatter
PARSER = argparse.ArgumentParser(description=__description__, formatter_class=formatter)
PARSER.add_argument('-y', '--yaml_file', type=str, required=True, default='CrabNebula_mod.yaml', help='name of the yaml file to read and dump on screen')

def main(**kwargs):
	input_filename=kwargs['yaml_file']
	with open(input_filename) as file:
	    # The FullLoader parameter handles the conversion from YAML
	    # scalar values to Python the dictionary format
	    input_list = yaml.load(file, Loader=yaml.FullLoader)

	    print(input_list)

if __name__ == '__main__':
    args = PARSER.parse_args()
    main(**args.__dict__)