#!/usr/bin/env python
# coding: utf-8

import argparse
import os
import glob
import yaml
import random

def launch_job_as_sh(list_of_files):
	#print(list_of_files)
	
	QUEUE="cta"
	print(f"echo \"{QUEUE}\"")
	os.system(f"echo \"{QUEUE}\"")

	QOPTION=" -app run_1h "
	#QOPTION=" -app run_24h "
	#QOPTION=" -app run_inf -m sl6_long "
	UNIQUE=f"{random.randint(0,10000)}_{random.randint(0,1000)}"
	HOMEWD=os.getcwd()
	TMPSCRIPT=(f"job_{UNIQUE}")
	
	JOBDIR=(f"{HOMEWD}/job_script_dir")
	LOGDIR=(f"{JOBDIR}/log")
	OUTDIR=(f"{HOMEWD}/Output_files_job")

	print(HOMEWD, UNIQUE, TMPSCRIPT,JOBDIR, LOGDIR, OUTDIR)
# =================
# === Main code ===
# =================

# --------------------------
# Adding the argument parser
arg_parser = argparse.ArgumentParser(description="""
This tools run the script hillas_preprocessing.py analysis step, with the bsub command to the CNAF CTA queue 
""")

arg_parser.add_argument("--config_G", default="config.yaml",
                        help='Global configuration file to steer the code execution.')
arg_parser.add_argument("--groups_of",type=int, required=False, default="1",
                        help='Number of files to be processed together.')
arg_parser.add_argument("--usereal",
                        help='Process only real data files.',
                        action='store_true')
arg_parser.add_argument("--usemc",
                        help='Process only simulated data files.',
                        action='store_true')
arg_parser.add_argument("--usetest",
                        help='Process only test files.',
                        action='store_true')
arg_parser.add_argument("--usetrain",
                        help='Process only train files.',
                        action='store_true')
arg_parser.add_argument("--usem1",
                        help='Process only M1 files.',
                        action='store_true')
arg_parser.add_argument("--usem2",
                        help='Process only M2 files.',
                        action='store_true')

parsed_args = arg_parser.parse_args()

def main():
	# ------------------------------
	# Reading the configuration file

	file_not_found_message = """
	Error: can not load the configuration file {:s}.
	Please check that the file exists and is of YAML format.
	Exiting.
	"""

	try:
	    config = yaml.load(open(parsed_args.config_G, "r"))
	except IOError:
	    print(file_not_found_message.format(parsed_args.config_G))
	    exit()


	print(f"{parsed_args.config_G} in groups of {parsed_args.groups_of}")

	config = yaml.load(open(parsed_args.config_G, "r"))

	if parsed_args.usereal and parsed_args.usemc:
	    data_type_to_process = config['data_files']
	elif parsed_args.usereal:
	    data_type_to_process = ['data']
	elif parsed_args.usemc:
	    data_type_to_process = ['mc']
	else:
	    data_type_to_process = config['data_files']

	if parsed_args.usetrain and parsed_args.usetest:
	    data_sample_to_process = ['train_sample', 'test_sample']
	elif parsed_args.usetrain:
	    data_sample_to_process = ['train_sample']
	elif parsed_args.usetest:
	    data_sample_to_process = ['test_sample']
	else:
	    data_sample_to_process = ['train_sample', 'test_sample']

	if parsed_args.usem1 and parsed_args.usem2:
	    telescope_to_process = ['magic1', 'magic2']
	elif parsed_args.usem1:
	    telescope_to_process = ['magic1']
	elif parsed_args.usem2:
	    telescope_to_process = ['magic2']
	else:
	    telescope_to_process = ['magic1', 'magic2']


	for data_type in data_type_to_process:
	    for sample in data_sample_to_process:
	    	for telescope in telescope_to_process:
	    		print(f"{telescope} {sample} {data_type}")
	    		print(f"{config['data_files'][data_type][sample][telescope]['input_mask']}")
	    		allinputfiles = glob.glob(f"{config['data_files'][data_type][sample][telescope]['input_mask']}")
	    		print(len(allinputfiles))
	    		for j in range(len(allinputfiles)//parsed_args.groups_of):
	    			print(j)
	    			#print(allinputfiles[j*parsed_args.groups_of:(j+1)*parsed_args.groups_of])
	    			launch_job_as_sh(allinputfiles[j*parsed_args.groups_of:(j+1)*parsed_args.groups_of])


if __name__ == '__main__':
    main()